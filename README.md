# Base layout with pug , js & scss configuration

## Usage:

* ```npm install ```   - install webpack & dependencies from 'package.json'
* ``` npm run dev ```  - development mod with hot-modules replacement
* ``` npm run build ```- build for production with compressing & uglify js, compile scss to css with prefixes
